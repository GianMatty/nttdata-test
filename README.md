# Nttdata Test ->  React + React Hooks + Fetch + Vite

Para comenzar utilizamos React, React Hooks, Fetch y Vite.

## Getting started

Se implemento el ejercicio tal como estaba indicado en el Documento PDF y se uso el servidor local enviado
para consumir los datos

## Rama "main" 

``` 
Implementación de una pagina de productos, se añadieron las observaciones: 

 - Se añadio la imagen de manera opcional si es que la api no lo manda
 - El cache dura 1 hora
 - Se añadio las etiquetas para almacenamiento y colores
 - Se corrigio y se controlo la respuesta de la api cuando el servidor esta caido
```

## Nota 

``` 
Para probar se uso un servidor local ya que el servidor online no funcionaba.
Asi que hay que levantar el servidor local apuntando a http://localhost:3000
 
```


## Installation

Clonar este repositorio y instalar las dependencias

```
 git clone https://gitlab.com/GianMatty/nttdata-test.git

 cd nttdata-test

 npm install
```

## Development

``` 
npm run dev 
```

## Build

```
npm run build
```

## Test

```
npm run test
```

## Lint

```
npm run lint
```
