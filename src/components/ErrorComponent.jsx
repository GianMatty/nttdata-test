import styles from './styles/error-component.module.css'

export const ErrorComponent = () => {
  return (
    <div className={styles.container}>
        <img className={styles.image} src="/src/assets/reparando2.png" alt="Reparando"/>
        <div className={styles.text}>Servicio No Disponible Por El Momento</div>
    </div>
  )
}

