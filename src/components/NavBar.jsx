import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { DataContext } from '../store';
import styles from './styles/navbar.module.css'

export const Navbar = () => {

    const { carCounter } = useContext( DataContext )
    const { updateDataProduct } = useContext( DataContext );

    const onHandleData = () => {
        updateDataProduct()
    }

    return (
        <nav className={styles.nav}>
            <Link className={styles.title} to="/" onClick={onHandleData}> MovilStore </Link> 
            <div className={styles.cart}>
                <i className="ri-shopping-cart-fill"></i>
                <div className={styles.counter}>{carCounter}</div>
            </div>
        </nav>
    )
}