import { Link } from 'react-router-dom';
import styles from './styles/card-movil.module.css'

export const CardMovil = ({product}) => {
  const {id, brand, model, price} = product;
  return (
    <div className={styles.card}>
      <img className={styles.image} src="/src/assets/telefono1.png" alt="telefono_image" />
      <div className={styles.description}>
        <label className={styles.title}>{brand} {model}</label>
        <div className={styles.price}>
          <label className={styles.subtitle}>Precio</label>
          <label className={styles.valor}>{price}</label>
        </div>
        <Link className={styles.detalle} to={`/detail/${ id }`}>
            Ver Detalle
        </Link>
      </div>
    </div>
  )
}

