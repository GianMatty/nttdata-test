import axios from "axios";

export const productsService = async() => {

    try {
        const url = `http://localhost:3001/api/product`
        const resp = await axios.get( url );
        const data = resp.data;
        return data;
    } catch (error) {
        return { error: error.code }
    }

}

export const productDetailService = async( idProduct ) => {

    try {
        const url = `http://localhost:3001/api/product/${idProduct}`
        const resp = await axios.get( url );
        const data = resp.data;
        
        return data;
    } catch (error) {
        return { error: error.code }
    }
}

export const buyProductService = async( product ) => {

    const url = `http://localhost:3001/api/cart`
    const resp = await fetch( url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(product)
    } );
    const data = await resp.json();
    return {data, "status": resp.status};
}
