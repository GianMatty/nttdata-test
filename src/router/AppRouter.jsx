import { Navigate, Route, Routes } from 'react-router-dom';
import { Navbar } from '../components';
import { DetailMovil, MovilStore } from '../pages';

export const AppRouter = () => {
  return (
    <>
        <Navbar />
        <Routes> 

            <Route path="store" element={ <MovilStore /> } />
            <Route path="detail/:id" element={ <DetailMovil /> } />

            <Route path="/*" element={<Navigate to="/store" />} />

        </Routes>
    </>
  )
}

