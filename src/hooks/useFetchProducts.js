import { useEffect, useState } from 'react';
import { productDetailService, productsService } from '../services/publicService';

export const useFetchProducts = ( category ) => {
 
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState( true );

    const getProducts = async() => {
        const getProducts = await productsService();
        setProducts(getProducts);
        setIsLoading(false);
    }
    
    useEffect( () => {
        getProducts();
    }, []);

    return {
        products,
        isLoading
    }
}

export const useFetchIdProduct = ( idProduct ) => {
 
    const [product, setProduct] = useState([]);
    const [isLoading, setIsLoading] = useState( true );

    const getProduct = async() => {
        const getProduct = await productDetailService(idProduct);
        setProduct(getProduct);
        setIsLoading(false);
    }
    
    useEffect( () => {
        getProduct();
    }, []);

    return {
        product,
        isLoading
    }
}