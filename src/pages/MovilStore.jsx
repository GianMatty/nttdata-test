
import { useContext, useEffect, useState } from "react";
import { CardMovil } from "../components";
import { ErrorComponent } from "../components/ErrorComponent";
import { DataContext } from "../store";
import styles from './styles/movil-store.module.css'

export const MovilStore = () => {

  const [ searchText, setSearchText ] = useState();
  const { getProducts, products } = useContext(DataContext)
  const [search, setSearch] = useState();
  const [error, setError] = useState(false);

  const onInputChange = ({ target }) => {
      const { value } = target;
      setSearchText(value);
  }
  
  useEffect(() => {
    if(searchText){
      const newSearch = searchFilter(products, searchText)
      setSearch(newSearch);
    }else {
      if(products.error !== 'ERR_NETWORK'){
        setSearch(products);
      }else {
        setError(true)
      }
    }
  },[searchText, products]);

  useEffect(() => {
    getProducts();
  },[]);

  const onSearchSubmit = (event) => {
    event.preventDefault();
    const newSearch = searchFilter(products, searchText)
    setSearch(newSearch);
  }

  const searchFilter = (products, seachText) => {
    const resp = products.filter( e => e.brand?.toLowerCase().includes(seachText.toLowerCase()) || 
                                      e.model?.toLowerCase().includes(seachText.toLowerCase()) );
    return resp;
  }

  return (
    <>
      {
        !error ?
          <div className={styles.container}>
              <div className={styles.search}>
                <div className={styles.bread}>catalogo</div>
                <form className={styles.form} onSubmit={ onSearchSubmit }>
                  <input 
                    type="text"
                    placeholder="Buscar Celular"
                    className={styles.form_control}
                    name="searchText"
                    autoComplete="off"
                    value={ searchText }
                    onChange={ onInputChange }
                  />
                  <button className={styles.searchButton} type='submit'>
                    <i className="ri-search-line"></i>
                  </button>
                </form>
              </div>
              <div className={styles.content}>
                {
                  search ? 
                  search.map(product => (
                    <CardMovil key={product.id} product={product}/>
                  )) :
                  products.map(product => (
                    <CardMovil key={product.id} product={product}/>
                  ))
                }
              </div>
            </div>
        :
            <ErrorComponent />
      }
    </>
  )
}
