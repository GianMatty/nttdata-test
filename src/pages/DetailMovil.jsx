import styles from './styles/detail-movil.module.css'
import Select from 'react-select'
import { useContext, useEffect, useState } from "react";
import { buyProductService } from "../services/publicService";
import { Link, useParams } from "react-router-dom";
import { DataContext } from "../store";
import { ErrorComponent } from '../components/ErrorComponent';

export const DetailMovil = () => {
  
  const { id } = useParams();
  const { getProduct, carCounterUpdate, productsDetail, updateDataProduct } = useContext( DataContext );

  const storagesSelect = productsDetail[0]?.options.storage.map( e => ({label: e.name, value: e.code}));
  const colorsSelect = productsDetail[0]?.options.color.map( e => ({label: e.name, value: e.code}));

  const [storage, setStorage] = useState();
  const [color, setColor] = useState();
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);


  useEffect(() => {
    getProduct(id);
  },[]);

  useEffect(() => {
    storagesSelect && setStorage(storagesSelect[0]);
    colorsSelect && setColor(colorsSelect[0]);
    if(productsDetail.error === "ERR_NETWORK"){
      setError(true)
    }
    if(productsDetail?.length !== 0){
      setLoading(false)
    }
  },[productsDetail]);

  const onDropdownStorageChange = (value) => {
    setStorage(value);
  }
  const onDropdownColorChange = (value) => {
    setColor(value);
  }

  const onHandleProduct = async(event) => {
    event.preventDefault();
    const addProduct = {
      id: productsDetail[0].id,
      colorCode: color?.value,
      storageCode: storage?.value 
    }
    const carCounter = await buyProductService(addProduct);
    if(carCounter.status === 200){
      carCounterUpdate(carCounter?.data.count);
    }
  }

  const onHandleData = () => {
    updateDataProduct()
  }

  return (
    <>
      <div className={styles.contentBread}>
        <Link className={styles.back} to="/store" onClick={onHandleData}>
          <i className="ri-arrow-left-fill" />
        </Link>
        <div className={styles.bread}>
          <Link className={styles.link} to="/store">catalogo</Link>
          <i className="ri-arrow-right-s-line"></i> detalle
        </div>
      </div>

      {
        loading ? <div className={styles.container_spinner}> <div className={styles.spinner}></div> </div>:
          !error ? 
              <div className={styles.container}>
                <img className={styles.image} src={ productsDetail[0]?.imgUrl ? productsDetail[0]?.imgUrl : "/src/assets/telefono2.png"} alt="image" />
                <div className={styles.content}>
                  <div className={styles.description}>
                    <label className={styles.headTitle}>Descripción</label>
                    <label className={styles.text}> <span className={styles.title}>Marca: </span> {productsDetail[0]?.brand}</label>
                    <label className={styles.text}> <span className={styles.title}>Modelo: </span> {productsDetail[0]?.model}</label>
                    <label className={styles.text}> <span className={styles.title}>Precio: </span> {productsDetail[0]?.price}</label>
                    <label className={styles.text}> <span className={styles.title}>CPU: </span> {productsDetail[0]?.cpu}</label>
                    <label className={styles.text}> <span className={styles.title}>RAM: </span> {productsDetail[0]?.ram}</label>
                    <label className={styles.text}> <span className={styles.title}>Sistema Operativo: </span> {productsDetail[0]?.so}</label>
                    <label className={styles.text}> <span className={styles.title}>Resolucion de pantalla: </span> {productsDetail[0]?.displayResolution}</label>
                    <label className={styles.text}> <span className={styles.title}>Bateria: </span> {productsDetail[0]?.battery}</label>
                    <label className={styles.text}> <span className={styles.title}>Camara Frontal: </span> {productsDetail[0]?.secondaryCamera}</label>
                    <label className={styles.text}> <span className={styles.title}>Camaras Trasera: </span> {productsDetail[0]?.primaryCamera}</label>
                    <label className={styles.text}> <span className={styles.title}>Dimensiones: </span> {productsDetail[0]?.dimentions}</label>
                    <label className={styles.text}> <span className={styles.title}>Peso: </span> {productsDetail[0]?.weight}</label>
                  </div>
                  <div className={styles.actions}>
                    <label className={styles.headTitle}>Acciones</label>
                    <form onSubmit={onHandleProduct} className={styles.form_group}>
                      <label style={{color: 'black', fontWeight: 600, fontSize: '14px'}}>Almacenamiento</label>
                      {
                        storagesSelect && 
                          <Select 
                          className={styles.drop}
                          defaultValue={ storagesSelect[0] }
                          options={storagesSelect}
                          onChange={onDropdownStorageChange}
                          />
                      }
                      <label style={{color: 'black', fontWeight: 600, fontSize: '14px', marginTop: '12px'}}>Color</label>
                      {
                        colorsSelect && 
                          <Select 
                            className={styles.drop}
                            defaultValue={ colorsSelect[0] }
                            options={colorsSelect}
                            onChange={onDropdownColorChange}
                          />
                      }
                      <button className={styles.button}>
                        Añadir
                      </button>
                    </form>
                  </div>
                </div>
              </div>
          : 
            <ErrorComponent />
      
      }

     
    </>
  )
}

