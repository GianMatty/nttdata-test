import { useReducer } from "react"
import { DataContext } from "./DataContext"
import dataReducer from "./dataReducer";
import { productDetailService, productsService } from "../services/publicService"

const init = () => {
    const cartProducts = JSON.parse(localStorage.getItem("productCart"));
    return {
        products: [],
        productsDetail: [],
        carCounter: cartProducts ? cartProducts : 0,
    }
}

export const DataProvider = ({ children }) => {

    const [ dataState, dispath ] = useReducer( dataReducer, {}, init );

    const productsExpired = (date) => {
        const fechaActual = new Date().getTime();
        const fechaRest = fechaActual - date;
        return fechaRest < 3600000; // Nota: Retorna "true" si aun no pasa la hora
    }

    const carCounterUpdate = ( counter = '' ) => {
        const cacheCounter = JSON.parse(localStorage.getItem( 'productCart' ))
        let counterUpdate = cacheCounter + counter;
        const action = {
            type: 'counter',
            payload: counterUpdate
        }
        localStorage.setItem( 'productCart', JSON.stringify( counterUpdate ) )
        dispath(action)
    }

    const getProducts = async () => {
        const cacheProducts = JSON.parse( localStorage.getItem('products') );
        const cacheOneHour = cacheProducts && productsExpired(cacheProducts.date)
        const products = (cacheProducts && cacheOneHour) ? cacheProducts.products : await productsService();
        const action = {
            type: 'getProducts',
            payload: products
        }
        if(products.error !== 'ERR_NETWORK'){
            !(cacheProducts && cacheOneHour) && localStorage.setItem( 'products', JSON.stringify( { products, date: new Date().getTime()} ) )
        }
        dispath(action)
    }

    const getProduct = async ( idProduct ) => {
        const cacheProduct = JSON.parse( localStorage.getItem('detailProduct') );
        const filterTimeCacheProduct = cacheProduct &&  cacheProduct.filter( e => productsExpired(e.date) )
        const existProduct = filterTimeCacheProduct && filterTimeCacheProduct.filter( e => e.id === idProduct);
        const product = (existProduct?.length > 0) ? existProduct : await productDetailService(idProduct);
        const productDate = [{...product[0], date: new Date().getTime()}]
        const action = {
            type: 'getProduct',
            payload: product
        }
        const newCacheProduct = (existProduct?.length > 0) ? filterTimeCacheProduct : (filterTimeCacheProduct ? [...filterTimeCacheProduct, ...productDate] : productDate );
        if(product.error !== 'ERR_NETWORK'){
            localStorage.setItem( 'detailProduct', JSON.stringify( newCacheProduct ) )
        }
        dispath(action)
            
    }

    const updateDataProduct = () => {
        const action = {
            type: 'getProduct',
            payload: []
        }
        dispath(action)
    }

    return (
        <DataContext.Provider value={{ ...dataState, carCounterUpdate, getProducts, getProduct, updateDataProduct }}>
            { children }
        </DataContext.Provider>
    )
}
