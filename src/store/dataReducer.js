
const dataReducer = ( state = {}, action) => {
  
    switch (action.type) {
        case 'counter':
            return {
                ...state,
                carCounter: action.payload
            }; 
        case 'getProducts':
            return {
                ...state,
                products: action.payload,
            };
        case 'getProduct':
            return {
                ...state,
                productsDetail: action.payload,
            };
    
        default:
            return state;
  }

}

export default dataReducer
