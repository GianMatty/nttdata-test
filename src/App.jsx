import './App.css'
import { AppRouter } from './router/AppRouter'
import { DataProvider } from './store'

function App() {

  return (
    <DataProvider>
      <AppRouter />
    </DataProvider>
  )
}

export default App
